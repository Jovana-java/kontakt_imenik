/*
 * Kreiranje funkcionalne web aplikacije na temu adresar firme, kontakt imenik.
 * Projekat KONTAKT_IMENIK
 * Jovana Savicevic
 */
package com.centroplan.baza;

import java.io.Serializable;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.sql.Connection;
import javax.annotation.PostConstruct;
import java.sql.SQLException;
/**
 * 
 * @author Jovana
 */
@Named("baza")
@SessionScoped
public class BazaBean implements Serializable{
    
    /**
     * Konekcija sa bazom.
     * Sve su private u Bean klasi.
     */
    //<editor-fold defaultstate="collapsed" desc="Konekcija sa bazom">
    private String PATH = "";
    private String HOST = "";
    private String PORT = "";
    private String USER = "";
    private String PASSWORD = "";
    private String fullUrl = "";
    
    private String porukaKonekcije = "";
    private static Connection connection = null;
    //</editor-fold>

    /**
     * Getter i setter metode
     * @return 
     */
    //<editor-fold defaultstate="collapsed" desc="GETTER i SEETER metode">
    public String getPATH() {
        return PATH;
    }

    public void setPATH(String PATH) {
        this.PATH = PATH;
    }

    public String getHOST() {
        return HOST;
    }

    public void setHOST(String HOST) {
        this.HOST = HOST;
    }

    public String getPORT() {
        return PORT;
    }

    public void setPORT(String PORT) {
        this.PORT = PORT;
    }

    public String getUSER() {
        return USER;
    }

    public void setUSER(String USER) {
        this.USER = USER;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }
    
    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getPorukaKonekcije() {
        return porukaKonekcije;
    }

    public void setPorukaKonekcije(String porukaKonekcije) {
        this.porukaKonekcije = porukaKonekcije;
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void setConnection(Connection connection) {
        BazaBean.connection = connection;
    }

//</editor-fold> 

    /**
     * Konstruktor default.
     * U Bean klasi obavezan.
     */
    public BazaBean() {
        
    }
    
    /**
     * Inicijalizacija.
     * PostConstruct - jednom i nikad vise.
     */
    @PostConstruct
    void init(){
        try {

            if ((connection == null) || (connection.isClosed())) {
                setConnection(Baza.getConnection());
            }
            else if (connection != null) {
                setPorukaKonekcije("Konekcija je uspela.");
            } else{
                setPorukaKonekcije("Nije uspela konekcija.");
            }
        } catch (SQLException se) {
            se.getMessage();
            setPorukaKonekcije("Nije uspela konekcija.");
        } catch (Exception e){
            e.getMessage();
        }
        String url = Baza.DRIVER_URL + Baza.HOST + ":" + Baza.PORT + "/" + Baza.DB_PATH;
        this.setFullUrl(url);
    }
}
