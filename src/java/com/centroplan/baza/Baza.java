/*
 * Kreiranje funkcionalne web aplikacije na temu adresar firme, kontakt imenik.
 * Projekat KONTAKT_IMENIK
 * Jovana Savicevic
 */
package com.centroplan.baza;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.sql.DriverManager;
/**
 * Konekcija sa bazom podataka, Firebird.
 * Final koristimo zato sto klasa sadrzi static metode 
 * i na taj nacin sprecavamo instanciranje.
 * @author Jovana
 */
public final class Baza {
    
    //<editor-fold defaultstate="collapsed" desc="DRIVER podaci za Firebird bazu">
    /**
     * DRIVER_CLASE_NAME za ucitavanje driver-a.
     * Pre konekcije sa bazom.  
     */
    public static final String DRIVER_CLASS_NAME = "org.firebirdsql.jdbc.FBDriver";
    
    /**
     * JDBC part url za Firebird bazu.
     */
    public static final String DRIVER_URL= "jdbc:firebirdsql://";
    
    /**
     * UTF8 charSet po default-u.
     */
    public static final String DEFAULT_CHARSET = "UTF8";
    
    /**
     * UTF8 encoding po default-u.
     */
    public static final String DEFAULT_ENCODING = "UTF8";
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Podaci baze i korisnika">
        /**
         * Test baze podataka.
         */
        public static final String DB_PATH = "C:/JovanaFbDb/KONTAKT_IMENIK.FDB";
        
        /**
         * Test host-a.
         */
        public static final String HOST = "localhost";
        
        /**
         * Test  port-a.
         */
        public static final String PORT = "3050";
        
        /**
         * Test korisnik, u ovom slucaju default.
         */
        public static final String USER = "SYSDBA";
        
        /**
         * Test password korisnika.
         */
        public static final String PASSWORD = "masterkey";
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="GET metode sa podacima o DRIVER-u">
    /**
     * Vraca String parametre za ucitavanje Driver-a.
     * @return 
     */ 
    public static String getDRIVER_CLASS_NAME() {
        return DRIVER_CLASS_NAME;
    }
    /**
     * Vraca String JDBC putanje tj. part (dela) putanje.
     * @return 
     */
    public static String getDRIVER_URL() {
        return DRIVER_URL;
    }

    public static String getDEFAULT_CHARSET() {
        return DEFAULT_CHARSET;
    }

    public static String getDEFAULT_ENCODING() {
        return DEFAULT_ENCODING;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="GET metode za podatke baze">

    /**
     * Vraca String tj. putanju do baze.
     * @return 
     */
    public static String getDB_PATH() {
        return DB_PATH;
    }
    
    /**
     * Vraca String tj. host za test-host.
     * @return 
     */
    public static String getHOST() {
        return HOST;
    }
    
    /**
     * Vraca String, tj. port za test-port.
     * @return 
     */
    public static String getPORT() {
        return PORT;
    }

    /**
     * Vraca String, tj. korisnika baze.
     * @return 
     */
    public static String getUSER() {
        return USER;
    }

    /**
     * Vraca String, tj. lozinku korisnika baze.
     * @return 
     */
    public static String getPASSWORD() {
        return PASSWORD;
    }
       
    //</editor-fold>

    /**
     * Private konstruktor, nije potrebno instanciranje.
     */
    private Baza() {
    }
    
    //<editor-fold defaultstate="collapsed" desc="Konekcija sa bazom">
     /**
     * Vraca konekciju sa bazom za test podatke.
     * @return 
     */
    public static Connection getConnection(){
        Connection connection = null;
        
        /**
         * Pokusaj ucitavanja driver-a.
         */
        try {
            Class.forName(DRIVER_CLASS_NAME);
        } catch (ClassNotFoundException ce) {
            ce.getMessage();
            return null;
        } catch (Exception ex) {
            ex.getMessage();
            return null;
        }
        
        /**
         * Pokusaj konekcije sa bazom.
         */
        try {
            String fullUrl = DRIVER_URL + HOST + ":" + PORT + "/" + DB_PATH;
            
            Properties props = new Properties();
            props.setProperty("user", USER);
            props.setProperty("password", PASSWORD);
            props.setProperty("charSet", DEFAULT_CHARSET);
            props.setProperty("encoding", DEFAULT_ENCODING);
            
            connection = DriverManager.getConnection(fullUrl,props); 
            if(connection!=null){
                System.out.println("Connection is OK...");
            } else {
                System.out.println("There is NO connection!");
            }
            
        } catch (SQLException se){
            se.getMessage();
            return null;
        } catch (Exception ex){
            ex.getMessage();
            return null;
        }
        return connection;
    }
    //</editor-fold>
    
}
